<?php

namespace Drupal\Tests\msgpack\Unit;

use Drupal\msgpack\Serialization\MessagePack;
use Drupal\Tests\UnitTestCase;

/**
 * MessagePack tests.
 */
class MessagePackTest extends UnitTestCase {

  /**
   * Test MessagePack encoding and decoding.
   */
  public function testMessagePack(): void {
    $object = $this->randomObject();
    $this->assertEquals($object, MessagePack::decode(MessagePack::encode($object)));
  }

  /**
   * Test MessagePack file extension.
   */
  public function testGetFileExtension(): void {
    $this->assertEquals('msgpack', MessagePack::getFileExtension());
  }

}
