<?php

namespace Drupal\msgpack\Serialization;

use Drupal\Component\Serialization\ObjectAwareSerializationInterface;
use Drupal\Component\Serialization\SerializationInterface;

/**
 * Provides a MessagePack serialization implementation.
 */
class MessagePack implements SerializationInterface, ObjectAwareSerializationInterface {

  /**
   * {@inheritdoc}
   */
  public static function encode($data): string {
    return msgpack_pack($data);
  }

  /**
   * {@inheritdoc}
   */
  public static function decode($raw) {
    return msgpack_unpack($raw);
  }

  /**
   * {@inheritdoc}
   */
  public static function getFileExtension(): string {
    return 'msgpack';
  }

}
